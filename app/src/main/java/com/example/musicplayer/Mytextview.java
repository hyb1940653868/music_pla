package com.example.musicplayer;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

/**
 * 继承AppCompatTextView，默认获得焦点
 */
public class Mytextview extends androidx.appcompat.widget.AppCompatTextView {
    public Mytextview(Context context) {
        super(context);
    }

    public Mytextview(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Mytextview(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean isFocused() {
        //自定义获取焦点
        return true;
    }
}
